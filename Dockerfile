FROM node:alpine as builder

WORKDIR '/app'

COPY package.json .
RUN npm install
COPY . .
RUN npm run build

FROM nginx
# location copying into is nginx specific
EXPOSE 80
COPY --from=builder /app/build /usr/share/nginx/html

#default command of nginx image will run it for us
